<?php
require 'connect.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Sản phẩm</title>
    <link rel="stylesheet" href="./assets/css/style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    <div class="main">
        <section class="header">
            <div class="row img__menu">
                <div class="col-lg-4 img__header">
                    <img src="./assets/img/header1.jpg" alt="ảnh coffee1" width="100%" height="150px">
                </div>
                <div class="col-lg-4 img__header">
                    <img src="./assets/img/header2.jpg" alt="ảnh coffee2" width="100%" height="150px">
                </div>
                <div class="col-lg-4 img__header">
                    <img src="./assets/img/header3.jpg" alt="ảnh coffee3" width="100%" height="150px">
                </div>
            </div>
            <nav class="nav-links">
                <ul>
                    <li><a href="./index.html">TRANG CHỦ</a></li>
                    <li><a href="#">GIỚI THIỆU</a></li>
                    <li><a href="./product.php">SẢN PHẨM</a></li>
                    <li><a href="#">LIÊN HỆ</a></li>
                </ul>
            </nav>
        </section>
        <section class="search container">
            <div class="row search__category">
                <div class="col-lg-3 all__category">
                    <div class="icon__category"><a href="#"><i class="fa fa-list"></i></a></div>
                    <div class="name__category"><a href="#">CÁC DANH MỤC</a></div>
                </div>
                <form class="col-lg-9 form-inline">
                    <input class="form-control mr-sm-2" type="search" placeholder="Nhập từ khóa..." aria-label="Search">
                    <button class="btn my-2 my-sm-0" type="submit"><i class="fa fa-search"></i></button>
                  </form>
            </div>
        </section>
        <section class="content m-3 p-4">
            <div class="row content__product ">
                <div class="col-lg-3 menu__left">
                    <div class="list__classify">
                        <h5 class="content__classify">PHÂN LOẠI</h5>
                        <ul class="items">
                            <li><a href="#" class="items__classify">HÃNG CAFE</a></li>
                            <li><a href="#" class="items__classify">LOẠI CAFE</a></li>
                            <li><a href="#" class="items__classify">CAFE</a></li>
                            <li><a href="#" class="items__classify">KHÁCH HÀNG</a></li>
                            <li><a href="#" class="items__classify">HÓA ĐƠN</a></li>
                            <li><a href="#" class="items__classify">DOANH NGHIỆP</a></li>
                            <li><a href="#" class="items__classify">THỊ TRƯỜNG</a></li>
                            <li><a href="#" class="items__classify">BLOG</a></li>
                            <li><a href="#" class="contact">LIÊN HỆ</a></li>
                        </ul>
                    </div>
                    <div class="content__contact">                        
                        <div class="text__contact">
                            <h5 class="">LIÊN HỆ</h5>
                        </div>
                        <div class="item__contact">
                            <div class="hotline">
                                <div class="icon"><i class="fa fa-volume-control-phone"></i></div>
                                <div class="text__hotline">Hotline</div>
                            </div>
                            <div class="call__hotline">
                                <h5>Call: 0853.798.492</h5>
                            </div>
                            <div class="support__online">
                                <div class="img__support">
                                    <img src="./assets/img/avt.jpg" alt="ảnh support">
                                </div>
                                <div class="text__support">
                                    <h6>HỖ TRỢ TRỰC TUYẾN</h6>
                                </div>
                            </div>
                            <div class="clickhere">
                                <div class="img__click">
                                    <img src="./assets/img/clickhere.jpg" alt="click">
                                </div>
                                <div class="text__click">
                                    <h6><a href="#">CLICK HERE</a></h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 col-md-9 col-sm-9">
                    <div class="row sale__product">
                        <div class="col-lg-12 sale__date my-3">
                            <div class="content__sale">
                                <h6>KHUYẾN MẠI GIỜ VÀNG</h6>
                            </div>
                            <div class="date__sale">
                                <h6>20/09-24/09</h6>
                            </div>
                        </div>
                        <div class="number__sale rounded-circle border border-white my-3">
                            <h5 class="number__sale--percent">30%</h5>
                        </div>
                    </div>
                    <div class="row add__product mt-4">
                        <a class="btn add-new" data-toggle="modal" data-target="#exampleModaladd">
                            <span>Thêm sản phẩm</span>
                        </a>
                    </div>
                    <div class="list__product">
                        <table class="table table-bordered">
                            <thead>
                                <tr class="items__product">
                                    <th class="text-center border-right text-dark">Mã Cafe</th>
                                    <th class="text-center border-right text-dark">Tên Cafe</th>
                                    <th class="text-center border-right text-dark">Trọng lượng</th>
                                    <th class="text-center border-right text-dark">Đơn giá</th>
                                    <th class="text-center border-right text-dark">Chức năng</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <?php
                                    $sql = "SELECT * FROM product";
                                    $result  = mysqli_query($conn, $sql);
                                    if ($result) {
                                        while ($row = mysqli_fetch_assoc($result)) {
                                    ?>
                                </tr>
                                <tr class="table onRow"> 
                                    <th><?php echo $row['Ma_CF']; ?></th>
                                    <td><?php echo $row['Ten_CF']; ?></td>
                                    <td><?php echo $row['Trong_luong']; ?></td>
                                    <td><?php echo $row['Don_gia']; ?></td>
                                    <td>
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModaledit" onClick="sua(this)" value=" <?php echo "`" . $row['Ma_CF'] . "`" . $row['Ten_CF'] . "`" . $row['Trong_luong'] . "`" . $row['Don_gia'] . "`"; ?> ">Sửa</button>
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModaldelete" onClick="xoa(this)" value="<?php echo "`" . $row['Ma_CF'] . "`"; ?>">Xóa</button>
                                </td>
                                </tr>
                                <?php
                                        }
                                    }
                                ?>
                            
                        </table>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-3">
                    <div class="img__body">
                        <div class="img__body--top">
                            <img src="./assets/img/banner1.png" alt="banner 1" class="img-fluid">
                        </div>
                        <div class="img__body--bottom">
                            <img src="./assets/img/banner2.png" alt="banner 2" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
            <footer>
                <div class="menu__footer">
                    <ul>
                        <li><a href="./index.html">TRANG CHỦ</a></li>
                        <li><a href="#">GIỚI THIỆU</a></li>
                        <li><a href="./product.php">SẢN PHẨM</a></li>
                        <li class="contact__footer"><a href="#">LIÊN HỆ</a></li>
                    </ul>
                    <div class="row content__footer">
                    <div class="col-lg-6 col-md-6 col-sm-6 content__footer--info">
                        <div class="office">
                            <div class="office__title">
                                <h6>Văn phòng: &ensp;</h6>
                            </div>
                            <div class="office__title--text">
                                <h6>1 Đ. Lê Đức Thọ, Mỹ Đình, Từ Liêm, Hà Nội, Việt Nam</h6>
                            </div>
                        </div>
                        <div class="phone">
                            <div class="phone__title">
                                <h6>Hotline: &ensp;</h6>
                            </div>
                            <div class="phone__title--text">
                               <h6>0853.798.492</h6>
                            </div>
                        </div>
                        <div class="email">
                            <div class="email__title">
                                <h6>Email:&ensp; </h6>
                            </div>
                            <div class="email__title--text">
                                <h6>trungudcbuic4k56@gmail.com
                                </h6>
                            </div>
                        </div>
                        <div class="mst">
                            <div class="mts__title">
                                <h6>MTS: &ensp;</h6>
                            </div>
                            <div class="mts__title--text">
                                <h6>0853.798.492</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 content__footer--license">
                        <h5>Trang web được xây dựng bởi Trung Đức Bùi<br> Hà Nội, ngày 25 tháng 09 năm 2022</h5>
                    </div>
                </div>
                </div>
                
            </footer>
        </section>
    </div>
    <!-- modal thêm -->
    <div class="modal" id="exampleModaladd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Thêm mới CAFE</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
            <form class="p-3" method="POST" action="./crud.php">
            <div class="mb-3">
                    <lable class="fom-lable">Mã Cafe</lable>
                    <input type="text" class="form-control mt-2" placeholder="Vd: CF_1, CF_2,..." required="" autocomplete="off" name="masp">
                </div>
                <div class="mb-3">
                    <lable class="fom-lable">Tên Cafe</lable>
                    <input type="text" class="form-control mt-2" placeholder="Tên sản phẩm" required="" autocomplete="off" name="tensp">
                </div>
                <div class="mb-3">
                    <lable class="fom-lable">Trọng lượng</lable>
                    <input type="number" class="form-control mt-2" min="1" placeholder="Nhập trọng lượng" required="" autocomplete="off" name="trongluong">
                </div>
                <div class="mb-3">
                    <lable class="fom-lable">Đơn giá</lable>
                    <input type="number" class="form-control mt-2" min="1" placeholder="Nhập đơn giá" required="" autocomplete="off" name="dongia">
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                <button type="submit" class="btn btn-primary" name="save">Lưu lại</button>
                </div>
            </form>
            </div>
        </div>
        </div>
    </div>

    <!-- modal sửa -->
    <div class="edit_product">
        <div class="modal" id="exampleModaledit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-body">
                    <div class="add-new-acc">
                    <h4 class="title-add-new p-2">Chỉnh sửa CAFE</h4>
                    <form class="p-3" method="POST" action="./crud.php">
                        <div class="mb-3">
                            <input type="text" id="macf" class="form-control mt-2" hidden name="idsp">
                        </div>
                        <div class="mb-3">
                            <lable class="fom-lable">Tên Cafe</lable>
                            <input type="text" id="tencf" class="form-control mt-2" name="namesp">
                        </div>
                        <div class="mb-3">
                            <lable class="fom-lable">Trọng lượng</lable>
                            <input type="number" id="tlg" min="1"  class="form-control mt-2" name="tlgsp">
                        </div>
                        <div class="mb-3">
                            <lable class="fom-lable">Đơn giá</lable>
                            <input type="number" id="dgia" min="1" class="form-control mt-2" name="dgiasp">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                            <button type="submit" name="capnhat" class="btn btn-primary" value="">Cập nhật</button>
                        </div>
                    </form>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modal xóa -->
    <div class="modal fade" id="exampleModaldelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Bạn có muốn xóa sản phẩm này hay không?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="./crud.php" method="POST">
                    <div class="modal-footer">
                        <input type="hidden" name="deleteid" id="delete_id">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                        <button type="submit" name="delete" class="btn btn-success" value="">Có, tôi muốn xóa</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
  <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
  <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
<script>
    function sua(a) {
        let mystring = a.value;
        let arrayStrig = mystring.split("`");
        document.getElementById('macf').value = arrayStrig[1];
        document.getElementById('tencf').value = arrayStrig[2];
        document.getElementById('tlg').value = arrayStrig[3];
        document.getElementById('dgia').value = arrayStrig[4];

    };
    function xoa(b){
        let myString = b.value;
        let arrayString = myString.split("`");
        document.getElementById('delete_id').value = arrayString[1];
    }
  </script>
</html>