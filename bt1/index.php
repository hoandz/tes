<?php
//including the database connection file
include_once("config.php");

//fetching data in descending order (lastest entry first)
$result = $dbConn->query("SELECT * FROM product ORDER BY id DESC");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Demo</title>
        <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css"
        />
        <link
            href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap"
            rel="stylesheet"
        />
        <link rel="stylesheet" href="style.css" />
    </head>
    <body>
        <div class="container">
            <div class="header">
                <img
                    src="./img/header.png"
                    class="header__img"
                    alt="image haeder"
                />
            </div>
            <div class="navbar">
                <div class="navbar__wrap">
                    <ul class="navbar__list">
                        <li class="navbar__link">
                            <a href="/bt1" class="active">Trang chủ</a>
                        </li>

                        <li class="navbar__link">
                            <a href="/bt1/add.html">Thêm sản phẩm</a>
                        </li>
                        <li class="navbar__link">
                            <a href="/bt1">Sản phẩm</a>
                        </li>
                        <li class="navbar__link">
                            <a href="/bt1">Liên hệ</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="main">
                <div class="main__wrapper">
                    <div class="list__category-wrap">
                        <div class="list__category-icon">
                            <i class="fa-solid fa-list"></i>
                            <span>Các danh mục</span>
                        </div>
                        <div class="list__category-input">
                            <input type="text" placeholder="Nhập từ khóa" />
                            <i class="fa-solid fa-magnifying-glass"></i>
                        </div>
                    </div>
                </div>
                <div class="main__body">
                    <div class="main__body-wrapper">
                        <div class="main__wrap">
                            <div class="classify">
                                <h2>Phân loại</h2>
                                <ul class="classify__list">
                                    <li class="classify__list-item">
                                        hãng cafe
                                    </li>
                                    <li class="classify__list-item">
                                        loại cafe
                                    </li>
                                    <li class="classify__list-item">cafe</li>
                                    <li class="classify__list-item">
                                        khách hàng
                                    </li>
                                    <li class="classify__list-item">hóa đơn</li>
                                    <li class="classify__list-item">
                                        doanh nghiệp
                                    </li>
                                    <li class="classify__list-item">
                                        thị trường
                                    </li>
                                    <li class="classify__list-item">blog</li>
                                    <li class="classify__list-item">liên hệ</li>
                                </ul>
                                <div class="contact">
                                    <h2>Liên hệ hỗ trợ</h2>
                                    <div class="contact__hotline">
                                        <i class="fa-solid fa-phone"></i>
                                        <span>Hotline</span>
                                    </div>
                                    <div class="contact__number-wrap">
                                        <p class="contact__number">
                                            Call: 1900.1008
                                        </p>
                                    </div>
                                    <div class="contact__support">
                                        <i
                                            class="fa-sharp fa-solid fa-user-tie"
                                        ></i>
                                        <span>Hỗ trợ trực tuyến </span>
                                    </div>
                                    <div class="contact__link">
                                        <i
                                            class="fa-solid fa-head-side-virus"
                                        ></i>
                                        <a href="/">Click here</a>
                                    </div>
                                </div>
                            </div>
                            <div class="content__center">
                                <div class="content__sale">
                                    <div class="content__sale-img">
                                        <img
                                            src="./img/sale.png"
                                            alt=""
                                            class="img__sale"
                                        />
                                    </div>
                                    <div class="product">

                                        <!-- start -->
                                        <?php 	
                                            while($row = $result->fetch(PDO::FETCH_ASSOC)) { 		
                                                echo '<div class="product__wrap">
                                                <img
                                                    src="./img/anh sp.png"
                                                    alt=""
                                                    class="product__img"
                                                />
                                                <div class="product__info">
                                                    <h3 class="product__name">
                                                        '.$row['name'].'
                                                    </h3>
    
                                                    <p class="product__price">
                                                        <span>Giá:</span>
                                                        <span class="underline">'.$row['price'].' VND</span>
                                                    </p>
                                                    <p class="product__still">
                                                        <span>Còn:</span>
                                                        '.$row['sale'].' VND
                                                    </p>
                                                    <div class="product__button">
                                                        <button>Mua ngay</button>
                                                        <button>
                                                            <a href="edit.php?id='.$row['id'].'">Xem chi tiết</a>
                                                        </button>
                                                        <button>
                                                            <a href="delete.php?id='.$row['id'].'">Xóa</a>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>';
                                            }
                                        ?>
                                        
                                        <!-- end -->
                                    </div>
                                </div>
                            </div>
                            <div class="banner__img">
                                <div class="banner__image">
                                    <img
                                        src="https://toigingiuvedep.vn/wp-content/uploads/2022/04/hinh-anh-ca-phe-mot-minh.jpg"
                                        alt=""
                                    />
                                    <img
                                        src="https://toigingiuvedep.vn/wp-content/uploads/2022/04/hinh-anh-ca-phe-mot-minh.jpg"
                                        alt=""
                                    />
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <div class="footer__wrap">
                                <div class="footer__top">
                                    <ul class="footer__list">
                                        <li class="footer__link">
                                            <a href="/" class="active"
                                                >Trang chủ</a
                                            >
                                        </li>
                                        <li class="footer__link">
                                            <a href="/">Giới thiệu</a>
                                        </li>
                                        <li class="footer__link">
                                            <a href="/">Sản phẩm</a>
                                        </li>
                                        <li class="footer__link">
                                            <a href="/">Liên hệ</a>
                                        </li>
                                    </ul>
                                    <div class="footer__info">
                                        <div class="footer__info-left">
                                            <div class="footer__info-leftWrap">
                                                <label for="">Văn phòng:</label>
                                                <span>92A Lê thanh nghị</span>
                                            </div>
                                            <div class="footer__info-leftWrap">
                                                <label for="">Email:</label>
                                                <span>92A Lê thanh nghị</span>
                                            </div>
                                            <div class="footer__info-leftWrap">
                                                <label for="">Văn phòng:</label>
                                                <span>92A Lê thanh nghị</span>
                                            </div>
                                            <div class="footer__info-leftWrap">
                                                <label for="">Văn phòng:</label>
                                                <span>92A Lê thanh nghị</span>
                                            </div>
                                        </div>
                                        <div class="footer__info-right">
                                            <p>
                                                Lorem ipsum dolor sit amet
                                                consectetur adipisicing elit.
                                                Fugit quas libero saepe incidunt
                                                aliquid atque unde nihil tempora
                                                quos sit quisquam quasi
                                                possimus, corporis, molestiae
                                                cum repellat cupiditate, nam
                                                earum.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
