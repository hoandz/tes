<html>
<head>
	<title>Thêm sản phẩm</title>
</head>

<body>
<?php
//including the database connection file
include_once("config.php");

if(isset($_POST['Submit'])) {	
	$name = $_POST['name'];
	$price = $_POST['price'];
	$sale = $_POST['sale'];
		
	$sql = "INSERT INTO product(name, price, sale) VALUES(:name, :price, :sale)";
    $query = $dbConn->prepare($sql);
            
    $query->bindparam(':name', $name);
    $query->bindparam(':price', $price);
    $query->bindparam(':sale', $sale);
    $query->execute();
    header("Location: index.php");
}
?>
</body>
</html>